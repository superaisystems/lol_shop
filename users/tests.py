from django.test import TestCase


# Create your tests here.

class TestUsers(TestCase):

    def test_signup_page_response(self):
        response = self.client.get('/users/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')
