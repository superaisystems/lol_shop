from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic
from .models import CustomUser
from .forms import CustomUserCreationForm


# Create your views here.

class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


def my_profile(request):
    if request.user.is_authenticated:

        context = {
            'my_orders': CustomUser.purchased_items
        }
        return render(request, 'my_profile.html', context)

    else:
        return render(request, 'registration/login.html')
