from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
# Create your models here.
from shop.models import Product

class CustomUserManager(UserManager):
    pass

class CustomUser(AbstractUser):
    purchased_items = models.ManyToManyField(Product, blank=True)

    def __str__(self):
        return self.username
    objects = CustomUserManager()
