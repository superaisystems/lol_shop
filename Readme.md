Guide for this project:

lol_shop is the main project files. 

'shop' app contains all product models, and urls to route to indiviual product pages.

'users' app contains customer user model, also contains all views and urls for log in, signup, log out
and gmail log in API details.

'cart' app will contain all models for an 'Order', an 'OrderItem' which will have a foreign key linking
to the Product model item. Will also contain a checkout and order summary.

To-do:

Make a payment app to handle payments from a checkout / completed order.'