from django.shortcuts import render, get_object_or_404, redirect
from .models import Product


# Create your views here.


def product_list_view(request):
    # use this function for home page to display all available items

    products = Product.objects.filter(available=True)

    context = {
        'products': products

    }

    return render(request, 'base.html', context)


def product_detail_view(request, id):
    # this function is for individual item pages.
    try:
        product = Product.objects.get(id=id)
        context = {
            'product': product
        }
        return render(request, 'detail.html', context)
    except Product.DoesNotExist:
        return redirect('/')


def about_view(request):
    context = {}
    return render(request, 'about.html', context)


def contact_view(request):
    context = {}
    return render(request, 'contact.html', context)
