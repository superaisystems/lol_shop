from django.test import TestCase
from .models import Product


# Create your tests here.

class Test_All_Urls_Return_200(TestCase):

    def test_home_product_page_response(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertEqual(response.status_code, 200)

    def test_product_page_response(self):
        response = self.client.get('/product/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertEqual(response.status_code, 200)

    def test_about_page_response(self):
        response = self.client.get('/about/')
        self.assertTemplateUsed(response, 'about.html')
        self.assertEqual(response.status_code, 200)

    def test_contact_page_response(self):
        response = self.client.get('/contact/')
        self.assertTemplateUsed(response, 'contact.html')
        self.assertEqual(response.status_code, 200)


class Test_Created_Products_Return_Correct_Response(TestCase):

    # Create two products to then test against
    def setUp(self):
        Product.objects.create(title="Rengar", price='12.99', stock='2')
        Product.objects.create(title="Taric", price='13.99', stock='5')

    # Test products exist
    def test_two_product_items_exist(self):
        all_products = Product.objects.all()
        self.assertEqual(all_products.count(), 2)

    # Test valid product returns detailed url page
    def test_product_id_returns_detail_page(self):
        response = self.client.get('/product/1/')
        self.assertTemplateUsed(response, 'detail.html')

    def test_incorrect_product_id_returns_redirect(self):
        # Product /4/ should not exist in test database
        response = self.client.get('/product/4/')
        if response == Exception:
            print("Exception, test passed")
        self.assertEqual(response.status_code, 302)
