from django.urls import path

app_name = 'shopping_cart'

from.views import (
add_to_cart,
delete_from_cart,
order_details,
checkout,
process_payment,
update_transaction_records,
success,
)

urlpatterns = [
    path('add-to-cart/<int:id>/', add_to_cart, name="add_to_cart"),
    path('order-summary/', order_details, name="order_summary"),
    path('success/', success, name="purchase_success"),
    path('item/delete/<int:id>/', delete_from_cart, name='delete_item'),
    path('checkout/', checkout, name="checkout"),
    path('payment/<int:order_id>/', process_payment, name="process_payment"),
    path('update-transaction/<int:order_id>/', update_transaction_records, name="update_records"),

]