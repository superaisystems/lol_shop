from django.shortcuts import render, redirect, reverse
from .models import OrderItem, Order
from shop.models import Product

import time, datetime
from users.models import CustomUser

from django.contrib import messages

# local
from .extras import generate_order_id


# Create your views here.
def get_user_pending_order(request):
    if request.user.is_authenticated:
        order = Order.objects.filter(owner=request.user, is_ordered=False)
        if order.exists():
            return order[0]
    else:
        return redirect('/accounts/login/')


def add_to_cart(request, id):
    if request.user.is_authenticated:
        # if logged in add to cart
        product = Product.objects.get(id=id)

        order_item, status = OrderItem.objects.get_or_create(product=product)
        print(order_item)
        user_order, status = Order.objects.get_or_create(owner=request.user)
        print(user_order.owner.username)
        user_order.items.add(order_item)
        user_order.save()
        print(user_order.get_cart_items())


        if status:
            # generate refcode
            user_order.ref_code = generate_order_id()
            user_order.save()



        messages.info(request, "item added to cart")
        return redirect(reverse('products:product_list'))


    else:
        messages.info(request, "You need to log in before you can do that! You are now being redirected! Please wait.")
        time.sleep(2)
        return redirect('/accounts/login/')


def delete_from_cart(request, id):
    item_to_delete = OrderItem.objects.filter(pk=id)
    if item_to_delete.exists():
        item_to_delete[0].delete()
        messages.info(request, 'Item has been deleted')
    return redirect(reverse('shopping_cart:order_summary'))


def order_details(request, id):
    if request.user.is_authenticated:
        existing_order = get_user_pending_order(request)
        context = {
            'order': existing_order,
        }
        return render(request, 'cart/order_summary.html', context)
    else:
        messages.info(request, "You need to log in before you can do that! You are now being redirected! Please wait.")
        return redirect('/accounts/login/')


def checkout(request):
    if request.user.is_authenticated:
        existing_order = get_user_pending_order(request)
        context = {
            'order': existing_order,
        }
        return render(request, 'cart/checkout.html', context)
    else:
        messages.info(request, "You need to log in before you can do that! You are now being redirected! Please wait.")
        return redirect('/accounts/login/')


def process_payment(request, order_id):
    # process payment
    return redirect(reverse('shopping_cart:update_records'), kwargs={
        'order_id': order_id
    })


def update_transaction_records(request, order_id):
    # get the order being processed
    order_to_purchase = Order.objects.filter(pk=order_id).first()

    # update placed order
    order_to_purchase.is_ordered = True
    order_to_purchase.date_ordered = datetime.datetime.now()
    order_to_purchase.save()

    # get all items in the order
    order_items = order_to_purchase.items.all()

    # update order items
    order_items.update(is_ordered=True, date_ordered=datetime.datetime.now())

    # Grab the user profile
    user_profile = CustomUser.objects.get(username=request.user.username)

    # get all the order items and add to the users purchased items
    order_products = [item.product for item in order_items]
    user_profile.purchased_items.add(*order_products)
    user_profile.save()

    """ 
    To do:
    1. Update payment records
    2. send email to customer confirming order
    3. send order details to site admin/ shipping to know an order needs to be shipped after payment received
    
    """
    messages.info(request, "Thankyou, your items have been added to your profile!")
    return redirect(reverse('users:my_profile'))


def success(request):
    pass
