from django.test import TestCase, Client
from shop.models import Product

# Create your tests here.
"""
To test:
    def get_user_pending_order
    - should return an existing order if object is true, or a redirect
        to '/accounts/login/'
        
    def add_to_cart(request, id)
    - if item has been added to cart will return redirect to
        'products:product_list'
    - else a redirect to '/accounts/login/'
    
    def delete_from_cart(request, id):
    - if item exists then delete from cart
    - THEN redirect to 'shopping_cart:order_summary'
    
    How am I going to write these tests?
    1. I want to visit the home page, not logged in.
    2. Then I want to log in
    3. Add an item to my cart
    4. Delete an item from my cart
    5. check my cart is empty
    
    """


class Test_Correct_Responses(TestCase):

    def setUp(self):
        first_item = Product.objects.create(title='test_poster_1', price=12.99,
                                            stock=5)
        second_item = Product.objects.create(title='test_poster_2', price=12.99,
                                             stock=3)

    def test_home_page_response_is_200(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='base.html')

    def test_two_poster_objects_exist(self):
        all_posters = Product.objects.all()
        self.assertEqual(all_posters.count(), 2)

    def test_login_page_post_works(self):
        c = Client()
        from users.models import CustomUser
        # create a new user to log in with
        test_user = CustomUser.objects.create(username='joshvo', email='joshvo@test1.com', password='password2')
        response = c.post('/accounts/login/', {'login': 'joshvo',
                                               'password': 'password2'})
        all_users = CustomUser.objects.all()  # grab all users
        self.assertEqual(all_users.count(), 1)  # test only my user has been created
        self.assertEqual(all_users[0].username, 'joshvo')  # test my user exists
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'base.html')

    def test_item_add_to_cart(self):
        pass
